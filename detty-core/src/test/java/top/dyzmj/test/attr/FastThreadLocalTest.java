package top.dyzmj.test.attr;

import org.hamcrest.CoreMatchers;
import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Timeout;
import top.dyzmj.detty.core.utils.concurrent.FastThreadLocal;
import top.dyzmj.detty.core.utils.concurrent.FastThreadLocalThread;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.jupiter.api.Assertions.*;

/**
 * 描述: {@link FastThreadLocal} 测试类
 *
 * @author dongYu
 * @date 2021/11/25
 */
public class FastThreadLocalTest {

	private static final FastThreadLocal<Integer> fastThreadLocal = new FastThreadLocal<>();

	private static void testOnRemoveCalled(boolean fastThreadLocal, final boolean callGet) throws Exception {

		final TestFastThreadLocal threadLocal = new TestFastThreadLocal();
		final TestFastThreadLocal threadLocal2 = new TestFastThreadLocal();

		Runnable runnable = () -> {
			if (callGet) {
				assertEquals(Thread.currentThread().getName(), threadLocal.get());
				assertEquals(Thread.currentThread().getName(), threadLocal2.get());
			} else {
				threadLocal.set(Thread.currentThread().getName());
				threadLocal2.set(Thread.currentThread().getName());
			}
		};
		Thread thread = fastThreadLocal ? new FastThreadLocalThread(runnable) : new Thread(runnable);
		thread.start();
		thread.join();

		String threadName = thread.getName();

		// Loop until onRemoval(...) was called. This will fail the test if this not works due a timeout.
		while (threadLocal.onRemovalCalled.get() == null || threadLocal2.onRemovalCalled.get() == null) {
			System.gc();
			System.runFinalization();
			Thread.sleep(50);
		}

		assertEquals(threadName, threadLocal.onRemovalCalled.get());
		assertEquals(threadName, threadLocal2.onRemovalCalled.get());
	}

	@Test
	void testUse() {

		new FastThreadLocalThread(() -> {
			for (int i = 0; i < 100; i++) {
				fastThreadLocal.set(i);
				System.out.println(Thread.currentThread().getName() + "====" + fastThreadLocal.get());
				try {
					Thread.sleep(200);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}, "FastThreadLocal-a").start();

		new FastThreadLocalThread(() -> {
			for (int i = 0; i < 100; i++) {
				System.out.println(Thread.currentThread().getName() + "====" + fastThreadLocal.get());
				try {
					Thread.sleep(200);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}, "FastThreadLocal-b").start();

	}

	@BeforeEach
	public void setUp() {
		FastThreadLocal.removeAll();
		MatcherAssert.assertThat(FastThreadLocal.size(), CoreMatchers.is(0));
	}

	@Test
	void testGetIfExists() {
		FastThreadLocal<Boolean> threadLocal = new FastThreadLocal<>() {
			@Override
			protected Boolean initialValue() {
				return Boolean.TRUE;
			}
		};

		assertNull(threadLocal.getIfExists());
		assertTrue(threadLocal.get());
		assertTrue(threadLocal.getIfExists());

		FastThreadLocal.removeAll();
		assertNull(threadLocal.getIfExists());
	}

	@Test
	@Timeout(value = 10000, unit = TimeUnit.MILLISECONDS)
	public void testRemoveAll() throws Exception {
		final AtomicBoolean removed = new AtomicBoolean();
		final FastThreadLocal<Boolean> var = new FastThreadLocal<Boolean>() {
			@Override
			protected void onRemoval(Boolean value) {
				removed.set(true);
			}
		};

		// Initialize a thread-local variable.
		assertThat(var.get(), is(nullValue()));
		assertThat(FastThreadLocal.size(), is(1));

		// And then remove it.
		FastThreadLocal.removeAll();
		assertThat(removed.get(), is(true));
		assertThat(FastThreadLocal.size(), is(0));
	}

	@Test
	@Timeout(value = 10000, unit = TimeUnit.MILLISECONDS)
	public void testRemoveAllFromFTLThread() throws Throwable {
		final AtomicReference<Throwable> throwable = new AtomicReference<Throwable>();
		final Thread thread = new FastThreadLocalThread() {
			@Override
			public void run() {
				try {
					testRemoveAll();
				} catch (Throwable t) {
					throwable.set(t);
				}
			}
		};

		thread.start();
		thread.join();

		Throwable t = throwable.get();
		if (t != null) {
			throw t;
		}
	}

	@Test
	@Timeout(value = 4000, unit = TimeUnit.MILLISECONDS)
	public void testOnRemoveCalledForFastThreadLocalGet() throws Exception {
		testOnRemoveCalled(true, true);
	}

	@Disabled("onRemoval(...) not called with non FastThreadLocal")
	@Test
	@Timeout(value = 4000, unit = TimeUnit.MILLISECONDS)
	public void testOnRemoveCalledForNonFastThreadLocalGet() throws Exception {
		testOnRemoveCalled(false, true);
	}

	@Test
	@Timeout(value = 4000, unit = TimeUnit.MILLISECONDS)
	public void testOnRemoveCalledForFastThreadLocalSet() throws Exception {
		testOnRemoveCalled(true, false);
	}

	@Disabled("onRemoval(...) not called with non FastThreadLocal")
	@Test
	@Timeout(value = 4000, unit = TimeUnit.MILLISECONDS)
	public void testOnRemoveCalledForNonFastThreadLocalSet() throws Exception {
		testOnRemoveCalled(false, false);
	}

	private static final class TestFastThreadLocal extends FastThreadLocal<String> {

		final AtomicReference<String> onRemovalCalled = new AtomicReference<String>();

		@Override
		protected String initialValue() {
			return Thread.currentThread().getName();
		}

		@Override
		protected void onRemoval(String value) {
			onRemovalCalled.set(value);
		}
	}

}
