package top.dyzmj.test.attr;

import org.junit.jupiter.api.Test;
import top.dyzmj.detty.core.utils.AttributeKey;
import top.dyzmj.detty.core.utils.DefaultAttributeMap;

public class AttributeTest {

	private static final AttributeKey<Device> ATTRIBUTE_KEY_DEVICE = AttributeKey.valueOf("device");
	private static final AttributeKey<Company> ATTRIBUTE_KEY_COMPANY = AttributeKey.valueOf("company");
	private static final AttributeKey<String> ATTRIBUTE_KEY_NET = AttributeKey.valueOf("net");

	@Test
	public void test01() {
		DefaultAttributeMap attribute = new DefaultAttributeMap();

		Device device = new Device("meter", 1);
		Company company = new Company("Goldcard", "300349");
		String net = "NB-IoT";

		attribute.attr(ATTRIBUTE_KEY_DEVICE).set(device);
		attribute.attr(ATTRIBUTE_KEY_COMPANY).set(company);
		attribute.attr(ATTRIBUTE_KEY_NET).set(net);

		Device device1 = attribute.attr(ATTRIBUTE_KEY_DEVICE).get();
		System.out.println(device1);

		boolean b = attribute.hasAttr(ATTRIBUTE_KEY_DEVICE);
		System.out.println(b);

	}

}

final class Device {
	private String name;
	private int id;

	public Device(String name, int id) {
		this.name = name;
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "Device{" +
				"name='" + name + '\'' +
				", id=" + id +
				'}';
	}
}

final class Company {
	private String name;
	private String code;

	public Company(String name, String code) {
		this.name = name;
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Override
	public String toString() {
		return "Company{" +
				"name='" + name + '\'' +
				", code='" + code + '\'' +
				'}';
	}
}
