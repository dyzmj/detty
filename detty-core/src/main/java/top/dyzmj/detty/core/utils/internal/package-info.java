/**
 * 描述: 仅限内部使用的实用程序，不允许在Netty外部使用。
 *
 * @author dongYu
 * @date 2021/11/25
 */
package top.dyzmj.detty.core.utils.internal;