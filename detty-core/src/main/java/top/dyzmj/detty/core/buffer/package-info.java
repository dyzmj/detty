/**
 * 描述: 字节缓冲区的抽象-表示低级二进制和文本消息的基本数据结构。
 *
 * @author dongYu
 * @date 2021/11/25
 */
package top.dyzmj.detty.core.buffer;