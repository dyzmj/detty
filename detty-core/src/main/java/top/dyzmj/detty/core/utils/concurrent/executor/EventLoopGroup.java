package top.dyzmj.detty.core.utils.concurrent.executor;

/**
 * 描述: 特殊的 {@link EventExecutorGroup} ，它允许注册通道，以便在事件循环期间进行后续选择。
 *
 * @author dongYu
 * @date 2021/11/26
 */
public interface EventLoopGroup extends EventExecutorGroup {

}
