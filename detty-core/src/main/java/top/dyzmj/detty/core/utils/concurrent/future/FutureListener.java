package top.dyzmj.detty.core.utils.concurrent.future;

/**
 * 描述:
 *
 * @author dongYu
 * @date 2021/11/29
 */
public interface FutureListener<V> extends GenericFutureListener<Future<V>> {
}
