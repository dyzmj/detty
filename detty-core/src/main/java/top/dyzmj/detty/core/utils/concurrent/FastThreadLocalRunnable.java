package top.dyzmj.detty.core.utils.concurrent;

import top.dyzmj.detty.core.utils.ObjectUtil;

/**
 * 描述: 实现 Runnable 接口，并提供包装方法
 *
 * @author dongYu
 * @date 2021/11/24
 */
final class FastThreadLocalRunnable implements Runnable {

	private final Runnable runnable;

	public FastThreadLocalRunnable(Runnable runnable) {
		this.runnable = ObjectUtil.checkNotNull(runnable, "runnable");
	}

	/**
	 * 将 {@link Runnable} 对象 包装成 {@link FastThreadLocalRunnable}
	 */
	static Runnable wrap(Runnable runnable) {
		return runnable instanceof FastThreadLocalRunnable ? runnable : new FastThreadLocalRunnable(runnable);
	}

	@Override
	public void run() {
		try {
			runnable.run();
		} finally {
			FastThreadLocal.removeAll();
		}
	}

}
