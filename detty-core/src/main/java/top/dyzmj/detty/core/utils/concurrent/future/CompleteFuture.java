package top.dyzmj.detty.core.utils.concurrent.future;

import top.dyzmj.detty.core.utils.concurrent.executor.EventExecutor;

import java.util.concurrent.TimeUnit;

/**
 * 描述: 一个骨架future实现，它代表一个已经完成的future
 *
 * @author dongYu
 * @date 2021/11/26
 */
public abstract class CompleteFuture<V> extends AbstractFuture<V> {

    private final EventExecutor executor;

    /**
     * 创建一个新实例。
     *
     * @param executor 与这个future关联的EventExecutor
     */
    protected CompleteFuture(EventExecutor executor) {
        this.executor = executor;
    }

    /**
     * 返回CompleteFuture使用的EventExecutor。
     */
    protected EventExecutor executor() {
        return executor;
    }

    @Override
    public Future<V> addListener(GenericFutureListener<? extends Future<? super V>> listener) {
        return null;
    }

    @Override
    public Future<V> addListeners(GenericFutureListener<? extends Future<? super V>>... listeners) {
        return null;
    }

    @Override
    public Future<V> removeListener(GenericFutureListener<? extends Future<? super V>> listener) {
        return this;
    }

    @Override
    public Future<V> removeListeners(GenericFutureListener<? extends Future<? super V>>... listeners) {
        return this;
    }

    @Override
    public Future<V> await() throws InterruptedException {
        if (Thread.interrupted()) {
            throw new InterruptedException();
        }
        return this;
    }

    @Override
    public boolean await(long timeout, TimeUnit unit) throws InterruptedException {
        if (Thread.interrupted()) {
            throw new InterruptedException();
        }
        return true;
    }

    @Override
    public Future<V> sync() throws InterruptedException {
        return this;
    }

    @Override
    public Future<V> syncUninterruptibly() {
        return this;
    }

    @Override
    public boolean await(long timeoutMillis) throws InterruptedException {
        if (Thread.interrupted()) {
            throw new InterruptedException();
        }
        return true;
    }

    @Override
    public Future<V> awaitUninterruptibly() {
        return this;
    }

    @Override
    public boolean awaitUninterruptibly(long timeout, TimeUnit unit) {
        return true;
    }

    @Override
    public boolean awaitUninterruptibly(long timeoutMillis) {
        return true;
    }

    @Override
    public boolean isDone() {
        return true;
    }

    @Override
    public boolean isCancellable() {
        return false;
    }

    @Override
    public boolean isCancelled() {
        return false;
    }

    /**
     * 试图取消此任务的执行。如果任务已经完成、已经取消或由于其他原因无法取消，则此尝试将失败。
     * 如果成功，且调用cancel时此任务尚未启动，则此任务将永远不会运行。
     * 如果任务已经启动，那么mayInterruptIfRunning参数确定是否应该中断执行该任务的线程以试图停止该任务。
     * 在这个方法返回之后，对isDone的后续调用将总是返回true。如果这个方法返回true，那么后续对isCancelled的调用将总是返回true。
     * 如果取消成功，它将通过CancellationException失败。
     *
     * @param mayInterruptIfRunning 这个值在这个实现中没有影响
     */
    @Override
    public boolean cancel(boolean mayInterruptIfRunning) {
        return false;
    }
}
