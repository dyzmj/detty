package top.dyzmj.detty.core.utils;

import top.dyzmj.detty.core.utils.internal.PlatformDependent;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.List;

/**
 * 描述:
 *
 * @author dongYu
 * @date 2021/11/29
 */
public class ThrowableUtil {

    private ThrowableUtil(){
    }

    /**
     * 使用类和方法名为给定的Throwable设置StackTraceElement。
     */
    public static <T extends Throwable> T unknownStackTrace(T cause, Class<?> clazz, String method) {
        cause.setStackTrace(new StackTraceElement[] { new StackTraceElement(clazz.getName(), method, null, -1)});
        return cause;
    }

    /**
     * 以字符串形式从可抛出对象获取堆栈跟踪。
     */
    public static String stackTraceToString(Throwable cause) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        PrintStream pout = new PrintStream(out);
        cause.printStackTrace(pout);
        pout.flush();
        try {
            return new String(out.toByteArray());
        } finally {
            try {
                out.close();
            } catch (IOException ignore) {
                // ignore as should never happen
            }
        }
    }

    public static boolean haveSuppressed() {
        return PlatformDependent.javaVersion() >= 7;
    }

    public static void addSuppressed(Throwable target, Throwable suppressed) {
        if (!haveSuppressed()) {
            return;
        }
        target.addSuppressed(suppressed);
    }

    public static void addSuppressedAndClear(Throwable target, List<Throwable> suppressed) {
        addSuppressed(target, suppressed);
        suppressed.clear();
    }

    public static void addSuppressed(Throwable target, List<Throwable> suppressed) {
        for (Throwable t : suppressed) {
            addSuppressed(target, t);
        }
    }

}
