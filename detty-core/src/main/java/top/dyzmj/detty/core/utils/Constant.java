package top.dyzmj.detty.core.utils;

/**
 * 一个可以通过==操作符进行比较的单例对象。
 * 由ConstantPool创建和管理。
 *
 * @param <T> 此对象可能与之比较的对象类型
 * @author dongYu
 * @date 2021/11/19
 */
public interface Constant<T extends Constant<T>> extends Comparable<T> {

	/**
	 * 返回分配给该常量的唯一编号
	 *
	 * @return 唯一编号
	 */
	int id();

	/**
	 * 返回该常量的名称。
	 *
	 * @return 名称
	 */
	String name();

}
