package top.dyzmj.detty.core.utils.concurrent.executor;

/**
 * 描述: {@link EventLoop} 的骨架实现。
 *
 * @author dongYu
 * @date 2021/11/26
 */
public abstract class AbstractEventLoop extends AbstractEventExecutor implements EventLoop {

	public AbstractEventLoop() {
	}

	public AbstractEventLoop(EventExecutorGroup parent) {
		super(parent);
	}

	@Override
	public EventLoopGroup parent() {
		return (EventLoopGroup) super.parent();
	}

	@Override
	public EventLoop next() {
		return (EventLoop) super.next();
	}

}
