/**
 * 描述: 用于并发/异步任务的实用程序类。
 *
 * @author dongYu
 * @date 2021/11/25
 */
package top.dyzmj.detty.core.utils.concurrent;