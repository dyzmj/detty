package top.dyzmj.detty.core.utils.concurrent.future;

import java.util.concurrent.TimeUnit;

/**
 * 描述: 异步操作的结果
 *
 * @author dongYu
 * @date 2021/11/26
 */
@SuppressWarnings("ClassNameSameAsAncestorName")
public interface Future<V> extends java.util.concurrent.Future<V> {

	/**
	 * 当且仅当I/O操作成功完成时返回true。
	 */
	boolean isSuccess();

	/**
	 * 当且仅当操作可以通过cancel取消时返回true(布尔值)。
	 */
	boolean isCancellable();

	/**
	 * 如果I/O操作失败，则返回失败的原因。
	 *
	 * @return 失败的原因。如果成功或此future尚未完成，则为Null。
	 */
	Throwable cause();

	/**
	 * 将指定的监听器添加到此future。
	 * 当这个future完成时，指定的监听器将被通知。
	 * 如果这个future已经完成，则立即通知指定的监听器。
	 */
	Future<V> addListener(GenericFutureListener<? extends Future<? super V>> listener);

	/**
	 * 将指定的多个监听器添加到此future。
	 * 当这个future完成时，指定的监听器将被通知。
	 * 如果这个future已经完成，则立即通知指定的监听器。
	 */
	Future<V> addListeners(GenericFutureListener<? extends Future<? super V>>... listeners);

	/**
	 * 从此future中删除指定侦听器的第一次出现。
	 * 当这个future完成时，指定的监听器不再被通知。
	 * 如果指定的侦听器与此future没有关联，则此方法不执行任何操作并静默返回。
	 */
	Future<V> removeListener(GenericFutureListener<? extends Future<? super V>> listener);

	/**
	 * 从此future中删除指定侦听器的第一次出现。
	 * 当这个future完成时，指定的监听器不再被通知。
	 * 如果指定的侦听器与此future没有关联，则此方法不执行任何操作并静默返回。
	 */
	Future<V> removeListeners(GenericFutureListener<? extends Future<? super V>>... listeners);

	/**
	 * 等待这个future，直到它完成，如果这个future失败，则重新抛出失败的原因。
	 */
	Future<V> sync() throws InterruptedException;

	/**
	 * 等待这个future，直到它完成，如果这个future失败，则重新抛出失败的原因。
	 */
	Future<V> syncUninterruptibly();

	/**
	 * 等待这个future完成
	 *
	 * @throws InterruptedException 如果当前线程被中断
	 */
	Future<V> await() throws InterruptedException;

	/**
	 * 等待这个future不间断地完成。
	 * 此方法捕获InterruptedException并静默地丢弃它
	 */
	Future<V> awaitUninterruptibly();

	/**
	 * 等待该future在指定的时间限制内完成。
	 *
	 * @return 当且仅当未来在指定的时间限制内完成时为
	 * @throws InterruptedException 如果当前线程被中断
	 */
	boolean await(long timeout, TimeUnit unit) throws InterruptedException;

	/**
	 * 等待该future在指定的时间限制内完成。
	 *
	 * @return 当且仅当未来在指定的时间限制内完成时为
	 * @throws InterruptedException 如果当前线程被中断
	 */
	boolean await(long timeoutMillis) throws InterruptedException;

	/**
	 * 等待该future在指定的时间限制内完成，不中断。此方法捕获InterruptedException并静默地丢弃它。
	 *
	 * @return 当且仅当未来在指定的时间限制内完成时为
	 */
	boolean awaitUninterruptibly(long timeout, TimeUnit unit);

	/**
	 * 等待该future在指定的时间限制内完成，不中断。此方法捕获InterruptedException并静默地丢弃它。
	 *
	 * @return 当且仅当未来在指定的时间限制内完成时为
	 */
	boolean awaitUninterruptibly(long timeoutMillis);

	/**
	 * 不阻塞地返回结果。
	 * 如果这个future还没有完成，这将返回null。
	 * 由于有可能使用空值来标记未来是否成功，您还需要使用 {@link #isDone()}检查未来是否真的完成，而不依赖于返回的空值。
	 */
	V getNow();

	/**
	 * 试图取消此任务的执行。
	 * 如果任务已经完成、已经取消或由于其他原因无法取消，则此尝试将失败。
	 * 如果成功，且调用cancel时此任务尚未启动，则此任务将永远不会运行。
	 * 如果任务已经启动，那么mayInterruptIfRunning参数确定是否应该中断执行该任务的线程以试图停止该任务。
	 * 在这个方法返回之后，对isDone的后续调用将总是返回true。
	 * 如果这个方法返回true，那么后续对isCancelled的调用将总是返回true。
	 * 如果取消成功，它将通过CancellationException失败。
	 */
	@Override
	boolean cancel(boolean mayInterruptIfRunning);
}
