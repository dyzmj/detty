package top.dyzmj.detty.core.utils.concurrent.executor;

/**
 * 描述: 想要支持调度的 {@link EventExecutor} 的抽象基类。
 *
 * @author dongYu
 * @date 2021/11/26
 */
public abstract class AbstractScheduledEventExecutor extends AbstractEventExecutor {



}
