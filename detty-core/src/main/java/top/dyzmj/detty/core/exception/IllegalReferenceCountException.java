package top.dyzmj.detty.core.exception;

/**
 * 描述: 当用户试图访问引用计数已经减少到0(并因此释放)的ReferenceCounted时，将引发IllegalStateException。
 *
 * @author dongYu
 * @date 2021/11/17
 */
public class IllegalReferenceCountException extends IllegalArgumentException {

	private static final long serialVersionUID = -2507492394288153468L;

	public IllegalReferenceCountException() {
	}

	public IllegalReferenceCountException(int refCnt) {
		this("refCnt: " + refCnt);
	}

	public IllegalReferenceCountException(int refCnt, int increment) {
		this("refCnt: " + refCnt + ", " + (increment > 0 ? "increment: " + increment : "decrement: " + -increment));
	}

	public IllegalReferenceCountException(String message) {
		super(message);
	}

	public IllegalReferenceCountException(String message, Throwable cause) {
		super(message, cause);
	}

	public IllegalReferenceCountException(Throwable cause) {
		super(cause);
	}

}
