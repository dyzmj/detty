package top.dyzmj.detty.core.utils;

/**
 * 一种允许存储值引用的属性。它可以以原子方式更新，因此是线程安全的。
 *
 * @param <T> 它所持有的值的类型。
 * @author dongYu
 * @date 2021/11/19
 */
public interface Attribute<T> {

	/**
	 * 返回该属性的键
	 */
	AttributeKey<T> key();

	/**
	 * 返回当前值，该值可能为空
	 */
	T get();

	/**
	 * 设置值
	 *
	 * @param value 要设置的值
	 */
	void set(T value);

	/**
	 * 以原子方式设置为给定值，并返回旧值，如果之前设置了non，则该值可能为空。
	 *
	 * @param value 要设置的值
	 * @return 旧值
	 */
	T getAndSet(T value);

	/**
	 * 如果该属性的值为空，则以原子方式设置为给定值。
	 * 如果不能设置值，因为它包含一个值，它将返回当前值。
	 *
	 * @param value 要设置的值
	 * @return 原值或当前给定的值
	 */
	T setIfAbsent(T value);

	/**
	 * 如果当前值==期望值，则以原子方式将值设置为给定的更新值。
	 * 如果设置成功，则返回true，否则返回false。
	 *
	 * @param oldValue 当前值
	 * @param newValue 期望值
	 * @return 结果
	 */
	boolean compareAndSet(T oldValue, T newValue);
}
