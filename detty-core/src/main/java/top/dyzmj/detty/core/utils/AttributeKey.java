package top.dyzmj.detty.core.utils;

/**
 * 键，可用于从AttributeMap中访问属性。请注意，不可能有多个具有相同名称的键。
 *
 * @author dongYu
 * @date 2021/11/19
 */
public final class AttributeKey<T> extends AbstractConstant<AttributeKey<T>> {

	private static final ConstantPool<AttributeKey<Object>> POOL = new ConstantPool<AttributeKey<Object>>() {
		@Override
		protected AttributeKey<Object> newConstant(int id, String name) {
			return new AttributeKey<>(id, name);
		}
	};

	/**
	 * 构造方法
	 */
	private AttributeKey(int id, String name) {
		super(id, name);
	}

	/**
	 * 返回具有指定名称的AttributeKey的单实例
	 */
	@SuppressWarnings("unchecked")
	public static <T> AttributeKey<T> valueOf(String name) {
		return (AttributeKey<T>) POOL.valueOf(name);
	}

	/**
	 * 如果给定名称存在AttributeKey则返回true。
	 */
	public static boolean exists(String name) {
		return POOL.exists(name);
	}

	/**
	 * 为给定名称创建一个新的AttributeKey，如果存在给定名称的AttributeKey，则会抛出IllegalArgumentException异常
	 */
	@SuppressWarnings("unchecked")
	public static <T> AttributeKey<T> newInstance(String name) {
		return (AttributeKey<T>) POOL.newInstance(name);
	}


}
