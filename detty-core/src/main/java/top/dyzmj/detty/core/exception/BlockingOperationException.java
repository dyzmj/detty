package top.dyzmj.detty.core.exception;

/**
 * 描述: 一个IllegalStateException，当用户在事件循环线程中执行阻塞操作时引发。
 * 如果在事件循环线程中执行阻塞操作，阻塞操作很可能会进入死锁状态，因此抛出此异常。
 *
 * @author dongYu
 * @date 2021/11/29
 */
public class BlockingOperationException extends IllegalStateException {

    private static final long serialVersionUID = 2462223247762460301L;

    public BlockingOperationException() {
    }

    public BlockingOperationException(String s) {
        super(s);
    }

    public BlockingOperationException(Throwable cause) {
        super(cause);
    }

    public BlockingOperationException(String message, Throwable cause) {
        super(message, cause);
    }

}
