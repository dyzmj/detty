package top.dyzmj.detty.core.buffer;

/**
 * 描述: 需要显式重分配的引用计数对象
 * <p>
 * 当实例化一个新的 {@link ReferenceCounted} 时，它从引用计数 {@code 1}开始。
 * {@link #retain()} 增加引用计数，{@link #release()} 减少引用计数。
 * 如果引用计数减少到 0，对象将被显式释放，而访问释放的对象通常会导致访问冲突。
 * </p>
 * <p>
 * 如果实现了 {@link ReferenceCounted} 的对象是实现了 {@link ReferenceCounted} 的其他对象的容器，
 * 那么当容器的引用计数变为 0 时，被包含的对象也将通过{@link #release()}被释放
 * </p>
 *
 * @author dongYu
 * @date 2021/11/11
 */
public interface ReferenceCounted {

    /**
     * 返回此对象的引用计数，如果为 0，则表示该对象已经被释放了。
     */
    int refCnt();

    /**
     * 将引用计数增加 1
     */
    ReferenceCounted retain();

    /**
     * 按指定的增量增加引用计数
     */
    ReferenceCounted retain(int increment);

    /**
     * 记录此对象的当前访问位置，已便调试
     */
    ReferenceCounted touch();

    /**
     * 用附加的任意信息记录此对象的当前访问位置，已便调试。
     */
    ReferenceCounted touch(Object hint);

    /**
     * 将引用计数减一，并在引用计数达到 0 时释放该对象
     *
     * @return 当且仅当引用计数为 0 ，且该对象已被释放时为真
     */
    boolean release();

    /**
     * 按照指定的递减量减少引用计数，并在引用计数达到 0 时释放该对象
     *
     * @return 当且仅当引用计数为 0 ，且该对象已被释放时为真
     */
    boolean release(int decrement);

}
