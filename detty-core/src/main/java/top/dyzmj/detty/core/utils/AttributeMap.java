package top.dyzmj.detty.core.utils;

/**
 * 保存可以通过AttributeKey访问的属性。实现必须是线程安全的。
 *
 * @author dongYu
 * @date 2021/11/19
 */
public interface AttributeMap {

	/**
	 * 获取给定AttributeKey的属性。
	 * 这个方法永远不会返回null，但是可能会返回一个还没有设置值的Attribute。
	 */
	<T> Attribute<T> attr(AttributeKey<T> key);

	/**
	 * 当且仅当给定的属性存在于此AttributeMap中时返回true。
	 */
	<T> boolean hasAttr(AttributeKey<T> key);

}
