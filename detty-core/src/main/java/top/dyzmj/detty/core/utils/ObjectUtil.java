package top.dyzmj.detty.core.utils;

/**
 * 描述: 对象工具类
 *
 * @author dongYu
 * @date 2021/11/18
 */
public class ObjectUtil {

	private ObjectUtil() {
	}

	/**
	 * 检查给定的参数是否不为空。如果是，抛出 {@link NullPointerException}。否则，返回实参
	 */
	public static <T> T checkNotNull(T arg, String text) {
		if (arg == null) {
			throw new NullPointerException(text);
		}
		return arg;
	}

}
