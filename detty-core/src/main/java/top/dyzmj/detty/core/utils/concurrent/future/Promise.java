package top.dyzmj.detty.core.utils.concurrent.future;

/**
 * 描述: 一个可写入的特殊的 {@link Future}
 *
 * @author dongYu
 * @date 2021/11/29
 */
public interface Promise<V> extends Future<V> {

    /**
     * 将此 {@link Future} 标记为成功并通知所有侦听器。
     * 如果已经成功或失败，它将抛出一个IllegalStateException
     */
    Promise<V> setSuccess(V result);

    /**
     * 将此 {@link Future} 标记为成功并通知所有侦听器
     *
     * @return 当且仅当成功标志着这个 {@link Future} 是成功的。
     * 否则是错误的，因为这个 {@link Future} 已经被标记为成功或失败。
     */
    boolean trySuccess(V result);

    /**
     * 将此 {@link Future} 标记为失败，并通知所有侦听器。
     * 如果已经成功或失败，它将抛出一个IllegalStateException。
     */
    Promise<V> setFailure(Throwable cause);

    /**
     * 将此 {@link Future} 标记为失败，并通知所有侦听器。
     *
     * @return 当且仅当成功标记此 {@link Future} 为失败时为True。
     * 否则是错误的，因为这个 {@link Future} 已经被标记为成功或失败。
     */
    boolean tryFailure(Throwable cause);

    /**
     * 让这个 {@link Future} 无法取消。
     *
     * @return 当且仅当成功地将该 {@link Future} 标记为不可取消，或该 {@link Future} 已经完成而未被取消时为True。
     * 如果这个 {@link Future} 已经被取消，则为False。
     */
    boolean setUncancellable();

    @Override
    Promise<V> addListener(GenericFutureListener<? extends Future<? super V>> listener);

    @Override
    Promise<V> addListeners(GenericFutureListener<? extends Future<? super V>>... listeners);

    @Override
    Promise<V> removeListener(GenericFutureListener<? extends Future<? super V>> listener);

    @Override
    Promise<V> removeListeners(GenericFutureListener<? extends Future<? super V>>... listeners);

    @Override
    Promise<V> await() throws InterruptedException;

    @Override
    Promise<V> awaitUninterruptibly();

    @Override
    Promise<V> sync() throws InterruptedException;

    @Override
    Promise<V> syncUninterruptibly();
}
