package top.dyzmj.detty.core.utils.concurrent.executor;

import top.dyzmj.detty.core.utils.concurrent.future.Promise;

/**
 * 描述: EventExecutor是一个特殊的eventexecuorgroup，它带有一些方便的方法来查看线程是否在事件循环中执行。除此之外，它还扩展了EventExecutorGroup以允许使用通用的方式访问方法。
 *
 * @author dongYu
 * @date 2021/11/26
 */
public interface EventExecutor extends EventExecutorGroup {

    @Override
    EventExecutor next();

    /**
     * 返回EventExecutorGroup，它是EventExecutor的父类
     */
    EventExecutorGroup parent();

    /**
     * 以 {@link Thread#currentThread()}作为参数调用 {@link #inEventLoop(Thread)}
     */
    boolean inEventLoop();

    /**
     * 如果给定的线程在事件循环中执行，返回true，否则返回false。
     */
    boolean inEventLoop(Thread thread);

    /**
     * 返回一个新的Promise
     */
    <V> Promise<V> newPromise();

}
