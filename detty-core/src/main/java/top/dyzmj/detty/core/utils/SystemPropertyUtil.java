package top.dyzmj.detty.core.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.regex.Pattern;

/**
 * 描述: 用于获取 Java 系统属性值
 *
 * @author dongYu
 * @date 2021/11/18
 */
public final class SystemPropertyUtil {

	private static final Logger logger = LoggerFactory.getLogger(SystemPropertyUtil.class);

	private static final Pattern INTEGER_PATTERN = Pattern.compile("-?[0-9]+");

	private SystemPropertyUtil() {
	}

	/**
	 * 返回带有指定的{@code key}的Java系统属性的值，如果属性访问失败则返回{@code null}。
	 */
	public static String get(String key) {
		return get(key, null);
	}

	/**
	 * 返回带有指定的{@code键}的Java系统属性的值，如果属性访问失败则返回指定的默认值。
	 */
	public static String get(final String key, String def) {
		if (key == null) {
			throw new NullPointerException("key");
		}
		if (key.isEmpty()) {
			throw new IllegalArgumentException("key must not be empty.");
		}

		String value = null;
		try {
			if (System.getSecurityManager() == null) {
				value = System.getProperty(key);
			} else {
				value = AccessController.doPrivileged((PrivilegedAction<String>) () -> System.getProperty(key));
			}
		} catch (Exception e) {
			logger.warn("Unable to retrieve a system property '{}'; default values will be used.", key, e);

		}
		if (value == null) {
			return def;
		}
		return value;
	}

	/**
	 * 返回带有指定键的Java系统属性的值，如果属性访问失败，则返回指定的默认值。
	 */
	public static boolean getBoolean(String key, boolean def) {
		String value = get(key);
		if (value == null) {
			return def;
		}

		value = value.trim().toLowerCase();
		if (value.isEmpty()) {
			return def;
		}

		if ("true".equals(value) || "yes".equals(value) || "1".equals(value)) {
			return true;
		}

		if ("false".equals(value) || "no".equals(value) || "0".equals(value)) {
			return false;
		}

		logger.warn(
				"Unable to parse the boolean system property '{}':{} - using the default value: {}",
				key, value, def
		);

		return def;
	}

	/**
	 * 返回带有指定键的Java系统属性的值，如果属性访问失败，则返回指定的默认值。
	 */
	public static int getInt(String key, int def) {
		String value = get(key);
		if (value == null) {
			return def;
		}
		value = value.trim().toLowerCase();
		if (INTEGER_PATTERN.matcher(value).matches()) {
			try {
				return Integer.parseInt(value);
			} catch (Exception e) {
				// Ignore
			}
		}
		return def;
	}

	/**
	 * 返回带有指定键的Java系统属性的值，如果属性访问失败，则返回指定的默认值。
	 */
	public static long getLong(String key, long def) {
		String value = get(key);
		if (value == null) {
			return def;
		}

		value = value.trim();
		try {
			return Long.parseLong(value);
		} catch (Exception e) {
			// Ignore
		}

		logger.warn(
				"Unable to parse the long integer system property '{}':{} - using the default value: {}",
				key, value, def
		);

		return def;
	}


}
