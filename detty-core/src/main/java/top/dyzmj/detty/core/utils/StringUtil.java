package top.dyzmj.detty.core.utils;

import static top.dyzmj.detty.core.utils.ObjectUtil.checkNotNull;

/**
 * 描述: 字符串工具类
 *
 * @author dongYu
 * @date 2021/11/18
 */
public class StringUtil {

	private static final char PACKAGE_SEPARATOR_CHAR = '.';

	private StringUtil() {
	}

	/**
	 * simpleClassName(o.getClass())的快捷方式
	 */
	public static String simpleClassName(Object o) {
		if (o == null) {
			return "null_object";
		} else {
			return simpleClassName(o.getClass());
		}
	}

	/**
	 * 从类生成简化的名称。类似于Class.getSimpleName()，但它适用于匿名类。
	 */
	public static String simpleClassName(Class<?> clazz) {
		String className = checkNotNull(clazz, "clazz").getName();
		final int lastDotIdx = className.lastIndexOf(PACKAGE_SEPARATOR_CHAR);
		if (lastDotIdx > -1) {
			return className.substring(lastDotIdx + 1);
		}
		return className;
	}
}
