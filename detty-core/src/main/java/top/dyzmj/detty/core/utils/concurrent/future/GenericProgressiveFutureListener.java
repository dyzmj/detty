package top.dyzmj.detty.core.utils.concurrent.future;

/**
 * 描述:
 *
 * @author dongYu
 * @date 2021/11/29
 */
public interface GenericProgressiveFutureListener<F extends ProgressiveFuture<?>> extends GenericFutureListener<F> {

    /**
     * 在操作进行时调用
     *
     * @param progress 迄今为止的操作进度(累计)
     * @param total    当进度到达时，表示操作结束的数字。-1如果操作结束未知
     * @throws Exception 异常
     */
    void operationProgressed(F future, long progress, long total) throws Exception;


}
