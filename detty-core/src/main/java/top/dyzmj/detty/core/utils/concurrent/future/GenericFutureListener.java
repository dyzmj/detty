package top.dyzmj.detty.core.utils.concurrent.future;

import java.util.EventListener;

/**
 * 描述: 侦听一个Future的结果。
 * 一旦通过调用 {@link Future#addListener(GenericFutureListener)} 添加了这个侦听器，异步操作的结果就会得到通知。
 *
 * @author dongYu
 * @date 2021/11/26
 */
public interface GenericFutureListener<F extends Future<?>> extends EventListener {

	/**
	 * 当与Future关联的操作完成时调用
	 */
	void operationComplete(F future);

}
