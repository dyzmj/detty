package top.dyzmj.detty.core.utils.internal;

/**
 * 描述: AA
 *
 * @author dongYu
 * @date 2021/11/24
 */
public abstract class TypeParameterMatcher {

	public TypeParameterMatcher() {
	}

	private static final TypeParameterMatcher NOOP = new TypeParameterMatcher() {
		@Override
		public boolean match(Object msg) {
			return true;
		}
	};

	/**
	 * 匹配
	 */
	public abstract boolean match(Object msg);

}
