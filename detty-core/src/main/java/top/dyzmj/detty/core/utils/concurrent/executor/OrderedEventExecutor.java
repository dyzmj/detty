package top.dyzmj.detty.core.utils.concurrent.executor;

/**
 * 描述: {@link EventExecutor} 的标记接口，它将以顺序/串行的方式处理所有提交的任务。
 *
 * @author dongYu
 * @date 2021/11/26
 */
public interface OrderedEventExecutor extends EventExecutor {



}
