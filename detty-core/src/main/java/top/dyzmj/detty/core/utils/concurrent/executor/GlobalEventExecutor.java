package top.dyzmj.detty.core.utils.concurrent.executor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import top.dyzmj.detty.core.utils.concurrent.future.Future;

import java.util.List;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * 描述: 单线程单例EventExecutor。
 * 它自动启动线程，并在任务队列中1秒内没有任务挂起时停止线程。
 * 请注意，将大量任务调度到该执行器是不可伸缩的;
 * 使用专门的执行者。
 *
 * @author dongYu
 * @date 2021/11/26
 */
public final class GlobalEventExecutor extends AbstractScheduledEventExecutor implements OrderedEventExecutor {

	public static final GlobalEventExecutor INSTANCE = new GlobalEventExecutor();
	private static final Logger logger = LoggerFactory.getLogger(GlobalEventExecutor.class);
	private static final long SCHEDULE_QUIET_PERIOD_INTERVAL = TimeUnit.SECONDS.toNanos(1);

	final BlockingQueue<Runnable> taskQueue = new LinkedBlockingQueue<>();
	volatile Thread thread;
	private final TaskRunner taskRunner = new TaskRunner();
	private final AtomicBoolean started = new AtomicBoolean();

	Runnable takeTask() {
		BlockingQueue<Runnable> taskQueue = this.taskQueue;
		for (; ; ) {

		}
	}

	@Override
	public boolean inEventLoop(Thread thread) {
		return thread == this.thread;
	}

	@Override
	public boolean isShuttingDown() {
		return false;
	}

	@Override
	public Future<?> shutdownGracefully(long quietPeriod, long timeout, TimeUnit unit) {
		return null;
	}

	@Override
	public Future<?> terminationFuture() {
		return null;
	}

	@Override
	public ScheduledFuture<?> schedule(Runnable command, long delay, TimeUnit unit) {
		return null;
	}

	@Override
	public <V> ScheduledFuture<V> schedule(Callable<V> callable, long delay, TimeUnit unit) {
		return null;
	}

	@Override
	public ScheduledFuture<?> scheduleAtFixedRate(Runnable command, long initialDelay, long period, TimeUnit unit) {
		return null;
	}

	@Override
	public ScheduledFuture<?> scheduleWithFixedDelay(Runnable command, long initialDelay, long delay, TimeUnit unit) {
		return null;
	}

	@Override
	public void shutdown() {

	}

	@Override
	public List<Runnable> shutdownNow() {
		return null;
	}

	@Override
	public boolean isShutdown() {
		return false;
	}

	@Override
	public boolean isTerminated() {
		return false;
	}

	@Override
	public boolean awaitTermination(long timeout, TimeUnit unit) {
		return false;
	}

	@Override
	public void execute(Runnable command) {

	}

	final class TaskRunner implements Runnable {
		@Override
		public void run() {

		}
	}

}
