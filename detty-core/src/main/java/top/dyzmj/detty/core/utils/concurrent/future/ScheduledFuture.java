package top.dyzmj.detty.core.utils.concurrent.future;

/**
 * 描述: 计划异步操作的结果。
 *
 * @author dongYu
 * @date 2021/11/29
 */
public interface ScheduledFuture<V> extends Future<V>, java.util.concurrent.ScheduledFuture<V> {
}
