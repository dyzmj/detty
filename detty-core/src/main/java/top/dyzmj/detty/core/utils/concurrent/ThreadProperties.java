package top.dyzmj.detty.core.utils.concurrent;

/**
 * 描述: 暴露线程的细节
 *
 * @author dongYu
 * @date 2021/11/26
 */
public interface ThreadProperties {

	/**
	 * @see Thread#getState()
	 */
	Thread.State state();

	/**
	 * @see Thread#getPriority()
	 */
	int priority();

	/**
	 * @see Thread#isInterrupted()
	 */
	boolean isInterrupted();

	/**
	 * @see Thread#isDaemon()
	 */
	boolean isDaemon();

	/**
	 * @see Thread#getName()
	 */
	String name();

	/**
	 * @see Thread#getId()
	 */
	long id();

	/**
	 * @see Thread#getStackTrace()
	 */
	StackTraceElement[] stackTrace();

	/**
	 * @see Thread#isAlive()
	 */
	boolean isAlive();

}
