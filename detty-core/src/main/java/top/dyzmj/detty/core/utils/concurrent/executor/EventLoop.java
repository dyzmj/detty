package top.dyzmj.detty.core.utils.concurrent.executor;

/**
 * 描述: 一旦注册，将处理通道的所有I/O操作。
 * 一个EventLoop实例通常会处理多个Channel，但这可能取决于实现细节和内部。
 *
 * @author dongYu
 * @date 2021/11/26
 */
public interface EventLoop extends OrderedEventExecutor, EventLoopGroup {

	@Override
	EventExecutorGroup parent();
}
