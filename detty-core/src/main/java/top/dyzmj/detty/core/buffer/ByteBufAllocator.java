package top.dyzmj.detty.core.buffer;

/**
 * 描述: 缓冲区分配器，实现负责分配缓冲区。这个接口的实现应该是线程安全的
 *
 * @author dongYu
 * @date 2021/11/11
 */
public interface ByteBufAllocator {

    /**
     * 分配一个 {@link ByteBuf} 缓冲区。是直接缓冲区还是堆缓冲区取决于实际的实现
     */
    ByteBuf buffer();

    /**
     * 使用给定的初始容量分配一个 {@link ByteBuf} 缓冲区。是直接缓冲区还是堆缓冲区取决于实际的实现
     */
    ByteBuf buffer(int initialCapacity);

    /**
     * 使用给定的初始容量和最大容量分配一个 {@link ByteBuf} 缓冲区。是直接缓冲区还是堆缓冲区取决于实际的实现
     */
    ByteBuf buffer(int initialCapacity, int maxCapacity);

    /**
     * 分配一个 {@link ByteBuf} 缓冲区，最好是一个适合I/O的直接缓冲区
     */
    ByteBuf ioBuffer();

    /**
     * 分配一个有初始容量的 {@link ByteBuf} 缓冲区，最好是一个适合I/O的直接缓冲区
     */
    ByteBuf ioBuffer(int initialCapacity);

    /**
     * 分配一个有初始容量和最大容量的 {@link ByteBuf} 缓冲区，最好是一个适合I/O的直接缓冲区
     */
    ByteBuf ioBuffer(int initialCapacity, int maxCapacity);

    /**
     * 分配一个堆 {@link ByteBuf} 缓冲区。
     */
    ByteBuf heapBuffer();

    /**
     * 分配一个有初始容量的堆 {@link ByteBuf} 缓冲区。
     */
    ByteBuf heapBuffer(int initialCapacity);

    /**
     * 分配一个有初始容量和最大容量的堆 {@link ByteBuf} 缓冲区。
     */
    ByteBuf heapBuffer(int initialCapacity, int maxCapacity);

    /**
     * 分配一个直接 {@link ByteBuf} 缓冲区。
     */
    ByteBuf directBuffer();

    /**
     * 分配一个有初始容量的直接 {@link ByteBuf} 缓冲区。
     */
    ByteBuf directBuffer(int initialCapacity);

    /**
     * 分配一个有初始容量和最大容量的直接 {@link ByteBuf} 缓冲区。
     */
    ByteBuf directBuffer(int initialCapacity, int maxCapacity);

    /**
     * 如果直接 {@link ByteBuf} 缓冲区被池化，则返回True
     */
    boolean isDirectBufferPooled();

    /**
     * 计算一个 {@link ByteBuf} 缓冲区需要扩展时使用的新容量。需要 minNewCapacity 以 maxCapacity 为上限。
     */
    int calculateNewCapacity(int minNewCapacity, int maxCapacity);

}
