package top.dyzmj.microbench.util;

import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.infra.Blackhole;
import org.openjdk.jmh.results.format.ResultFormatType;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.util.concurrent.TimeUnit;

/**
 * 描述:
 *
 * @author dongYu
 * @date 2021/11/23
 */
@BenchmarkMode(Mode.AverageTime)
@Warmup(iterations = 5, time = 1)
@Measurement(iterations = 5, time = 5)
@Threads(4)
@Fork(1)
@State(value = Scope.Benchmark)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
public class BitCalcBenchmark {

	@Param(value = {"100000", "1000000", "10000000"})
	private long length;

	@Benchmark
	public void testMod(Blackhole blackhole){

		int num = 0;
		for (int i = 0; i < length; i++){
			num = 9999 % 1024;
		}
		blackhole.consume(num);
	}

	@Benchmark
	public void testOffset(Blackhole blackhole){
		int num = 0;
		for (int i = 0; i<length; i++){
			num = 9999 << 10;
		}
		blackhole.consume(num);
	}

	public static void main(String[] args) throws RunnerException {
		Options opt = new OptionsBuilder().include(BitCalcBenchmark.class.getSimpleName())
				.result("result.json")
				.resultFormat(ResultFormatType.JSON).build();
		new Runner(opt).run();
	}

}
